Manual:

Ett par .txt filer medföljer med färdiga labyrinter som går att lösa.
-----------------------------------------------------------------------------------

KÖRA IGÅNG JUNIT:

För att köra igång JUnit testet välj textfilen maze3.txt. Detta görs i klassen
"Driver" i paketet "mazePathFinder". Ändra indatat till "mazefile" variabeln till
den önskade textfilen.
-----------------------------------------------------------------------------------

LÖSA SLUMPMÄSSIG LABYRINT:

För att köra igång en slumpmässig labyrint, använd maze4.txt. Detta görs i klassen
"Driver" i paketet "mazePathFinder". Ändra indatat till "mazefile" variabeln till
den önskade textfilen.
------------------------------------------------------------------------------------

GENERERA SLUMPMÄSSIG LABYRINT:

För att skapa en ny slumpmässig labyrint, kör igång klassen "Main" i paketet
MazeGenerator. Välj längden och höjden på den labyrint som ni vill skall
genereras och sedan kommer denna labyrint att automatiskt sparas i maze4.txt filen
samt skrivas ut till konsolen.

------------------------------------------------------------------------------------

RITA EN EGEN LABYRINT:

För att rita en egen labyrint, skall det göras i en textfil (.txt). En vägg i labyrinten
representeras med tecknet "#". Starten och slutet av labyrinten representeras av
tecknen "S" respektive "E". En vanlig väg som går att ta representeras endast av ett 
mellanslag (space). Krav på labyrinten är att det skall finnas minst en väg som löser
labyrinten, max 1 startpunkt respektive slutpunkt och att längden av väggen längst upp
och längden på väggen längst ned är lika stora.