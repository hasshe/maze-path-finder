package mazePathFinder;
import java.io.File;

import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;


public class Maze {
    private static final int AVAILABLE_ROAD = 0;
    private static final int IS_WALL = 1;
    private static final int IS_START = 2;
    private static final int IS_EXIT = 3;
    private static final int IS_PATH = 4;

    private static int[][] maze;
    private static boolean[][] visited;
    private static Coordinate start;
    private static Coordinate exit;
    private static int pathLength = 0;

    /**
     * Reads a textfile for creating a maze
     * @param mazeFile input is textfile with the maze for creation
     * @throws FileNotFoundException if file is not found or does not exist
     */
    public Maze(File mazeFile) throws FileNotFoundException {
        String mazeFileData = "";
        Scanner mazeFileInput = new Scanner(mazeFile); 
            
        while (mazeFileInput.hasNextLine()) {              
        	mazeFileData += mazeFileInput.nextLine() + "\n";
        }
        createMaze(mazeFileData);
    }
/**
 * Initializes the maze by reading the read lines from the textfiles
 * @param mazeFileData text from the read textfile
 */
    private void createMaze(String mazeFileData) {
        if (mazeFileData == null || (mazeFileData = mazeFileData.trim()).length() == 0) {
            throw new IncorrectMazeArguments("Illegal argument! Check maze text file");
        }

        String[] mazeFileDataLines = mazeFileData.split("[\r]?\n");
        maze = new int[mazeFileDataLines.length][mazeFileDataLines[0].length()];
        visited = new boolean[mazeFileDataLines.length][mazeFileDataLines[0].length()];

        for (int row = 0; row < getMazeHeight(); row++) {
            if (mazeFileDataLines[row].length() != getMazeWidth()) {
                throw new IncorrectMazeArguments("Illegal argument! Check maze length");
            }

            for (int column = 0; column < getMazeWidth(); column++) {
                if (mazeFileDataLines[row].charAt(column) == '#') {
                    maze[row][column] = IS_WALL;
                }
                
                else if (mazeFileDataLines[row].charAt(column) == 'S') {
                    maze[row][column] = IS_START;
                    start = new Coordinate(row, column);
                } 
                
                else if (mazeFileDataLines[row].charAt(column) == 'E') {
                    maze[row][column] = IS_EXIT;
                    exit = new Coordinate(row, column);
                } 
                
                else {
                    maze[row][column] = AVAILABLE_ROAD;
                }
            }
        }
    }
/**
 * 
 * @return height of the maze
 */
    public int getMazeHeight() {
        return maze.length;
    }
/**
 * 
 * @return breadth of the maze
 */
    public int getMazeWidth() {
        return maze[0].length;
    }
/**
 * 
 * @return starting coordinates in maze
 */
    public Coordinate getStartCoordinates() {
        return start;
    }
/**
 * 
 * @return ending coordinates in maze
 */
    public Coordinate getExitCoordinates() {
        return exit;
    }
/**
 * 
 * @param xCoordinate x coordinates for end
 * @param yCoordinate y coordinates for end
 * @return if end is reached
 */
    public boolean isExitCoordinates(int xCoordinate, int yCoordinate) {
        return xCoordinate == exit.getXCoordinate() && yCoordinate == exit.getYCoordinate();
    }
/**
 * 
 * @param xCoordinate x coordinates for start
 * @param yCoordinate y coordinates for start
 * @return if at start is true or false
 */
    public boolean isStartCoordinates(int xCoordinate, int yCoordinate) {
        return xCoordinate == start.getXCoordinate() && yCoordinate == start.getYCoordinate();
    }
/**
 * 
 * @param row a row index
 * @param column a column index
 * @return current coordinates are visited
 */
    public boolean visitedCoordinates(int row, int column) {
        return visited[row][column];
    }
/**
 * 
 * @param row a row index
 * @param column a column index
 * @return true or false if the coordinate is a wall or not
 */
    public boolean isWall(int row, int column) {
        return maze[row][column] == IS_WALL;
    }
/**
 * 
 * @param row an index for a row
 * @param column an index for a column
 * @param status true or false if coordinates have been visited
 */
    public void setCoordinatesVisited(int row, int column, boolean status) {
        visited[row][column] = status;
    }
/**
 * 
 * @param row an index for a row
 * @param column an index for a column
 * @return if coordinates are within the bounds of the maze
 */
    public boolean isLocationValid(int row, int column) {
        if (row < 0 || row >= getMazeHeight() || column < 0 || column >= getMazeWidth()) {
            return false;
        }
        return true;
    }
/**
 * 
 * @param path list of coordinates that are valid. Includes walls, start point, end point and availiable paths.
 */
    public void pathPrinter(List<Coordinate> path) {
        int[][] tempMaze = Arrays.stream(maze).map(int[]::clone).toArray(int[][]::new);
        for (Coordinate coordinate : path) {
            if (isStartCoordinates(coordinate.getXCoordinate(), coordinate.getYCoordinate()) || 
            		isExitCoordinates(coordinate.getXCoordinate(), coordinate.getYCoordinate())) {
                continue;
            }
            tempMaze[coordinate.getXCoordinate()][coordinate.getYCoordinate()] = IS_PATH;
        }
        System.out.println(toString(tempMaze));
        
    }
/**
 * 
 * @param maze array with the coordinates for the maze
 * @return a string of the array for printing
 */
    public String toString(int[][] maze) {
        StringBuilder result = new StringBuilder(getMazeWidth() * (getMazeHeight() + 1));
        for (int row = 0; row < getMazeHeight(); row++) {
            for (int column = 0; column < getMazeWidth(); column++) {
                if (maze[row][column] == AVAILABLE_ROAD) {
                    result.append("   ");
                } 
                
                else if (maze[row][column] == IS_WALL) {
                    result.append("[#]");
                }
                
                else if (maze[row][column] == IS_START) {
                    result.append(" S ");
                }
                
                else if (maze[row][column] == IS_EXIT) {
                    result.append(" E ");
                }
                
                else {
                	pathLength ++;
                    result.append(" . ");
                }
            }
            result.append('\n');
        }
        return result.toString();
    }
    public int fastestPathLength() {
    	int r = pathLength;
    	pathLength = 0;
    	return r;
    }
}