package mazePathFinder;
public class Coordinate {
   protected int xCoordinate;
   protected int yCoordinate;
   protected Coordinate next;
/**
 * 
 * @param x x axis coordinate value
 * @param y y axis coordinate value
 */
    public Coordinate(int x, int y) {
        this.xCoordinate = x;
        this.yCoordinate = y;
        this.next = null;
    }
/**
 * 
 * @param xCoordinate x axis coordinate value
 * @param yCoordinate y axis coordinate value
 * @param next next coordinate value
 */
    public Coordinate(int xCoordinate, int yCoordinate, Coordinate next) {
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.next = next;
    }
/**
 * 
 * @return x axis value
 */
   protected int getXCoordinate() {
        return xCoordinate;
    }
/**
 * 
 * @return y axis value
 */
   protected int getYCoordinate() {
        return yCoordinate;
    }
/**
 * 
 * @return next coordinates
 */
    protected Coordinate getNext() {
        return next;
    }
}