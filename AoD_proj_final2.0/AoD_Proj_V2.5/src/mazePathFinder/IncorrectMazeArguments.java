package mazePathFinder;


/**
 * Exception class that signals if an operation that is not allowed has been
 * done on an empty list.
 * 
 * @author Magnus Blom
 */
@SuppressWarnings ("serial")
public class IncorrectMazeArguments extends RuntimeException
{
	/**
	 * A constructor tha takes a message about which error has been generated.
	 * This can be written to the user when the exception is catched.
	 * @param message error message to display
	 */
	public IncorrectMazeArguments (String message)
	{
		// Call the parents constructor.
		super (message);
	}
}
