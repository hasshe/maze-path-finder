package mazePathFinder;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 *This class sends a text file with the maze data to the Maze class and then conducts the BFS algorithm
 *
 */
public class Driver {
	
	private static final int[][] DIRECTIONS = { { 0, 1 }, { 1, 0 }, { 0, -1 }, { -1, 0 } };
	private static Maze maze;
	private static File mazeFile;
	
	public Driver () throws FileNotFoundException {
		
    mazeFile = new File("src/maze4.txt");
    //Ta bort kommentaren under och kommentera raden ovan för att köra igång unit testet
   // mazeFile = new File("src/maze4.txt"); 

	maze = new Maze(mazeFile);

    List<Coordinate> fastestPath = bfsPathFinder(maze);
    maze.pathPrinter(fastestPath);
    System.out.println("Path Length: " + maze.fastestPathLength() + "\n");
    
    
}

    /**
     * 
     * @param maze data for the current maze that was read from the text file
     * @return the emptied list
     */
    public List<Coordinate> bfsPathFinder(Maze maze) {
        LinkedList<Coordinate> nextCoordinate = new LinkedList<>();
        Coordinate start = maze.getStartCoordinates();
        nextCoordinate.add(start);

        while (nextCoordinate.isEmpty()==false) {
            Coordinate currentCoordinate = nextCoordinate.remove();

            if (maze.isLocationValid(currentCoordinate.getXCoordinate(), currentCoordinate.getYCoordinate())==false || maze.visitedCoordinates(currentCoordinate.getXCoordinate(), currentCoordinate.getYCoordinate())==true) {
                continue;
            }

            if (maze.isWall(currentCoordinate.getXCoordinate(), currentCoordinate.getYCoordinate())==true) {
                maze.setCoordinatesVisited(currentCoordinate.getXCoordinate(), currentCoordinate.getYCoordinate(), true);
                continue;
            }

            if (maze.isExitCoordinates(currentCoordinate.getXCoordinate(), currentCoordinate.getYCoordinate())==true) {
                return backtrackPath(currentCoordinate);
            }

            for (int[] direction : DIRECTIONS) {
                Coordinate coordinate = new Coordinate(currentCoordinate.getXCoordinate() + direction[0], currentCoordinate.getYCoordinate() + direction[1], currentCoordinate);
                nextCoordinate.add(coordinate);
                maze.setCoordinatesVisited(currentCoordinate.getXCoordinate(), currentCoordinate.getYCoordinate(), true);
            }
        }
        return null;
    }
/**
 * 
 * @param currentCoordinate the current coordinates of the x and y axis where the exit point is
 * @return the fastest path through the maze
 */
    private List<Coordinate> backtrackPath(Coordinate currentCoordinate) {
        List<Coordinate> path = new ArrayList<>();
        Coordinate iterate = currentCoordinate;

        while (iterate != null) {
            path.add(iterate);
            iterate = iterate.next;
        }

        return path;
    }
}
