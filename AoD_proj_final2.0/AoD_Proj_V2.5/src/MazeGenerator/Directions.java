package MazeGenerator;

/**
 * 
 * Defines directions and their opposites for the traversal of the maze creation
 * Source: http://weblog.jamisbuck.org/2010/12/27/maze-generation-recursive-backtracking
 */

public enum Directions {
	
	NORTH(1, 0, -1), 
	SOUTH(2, 0, 1), 
	EAST(4, 1, 0), 
	WEST(8, -1, 0);
	
	protected int current;
	protected int directionX;
	protected int directionY;
	protected Directions directionOpposite;

	static {
		NORTH.directionOpposite = SOUTH;
		SOUTH.directionOpposite = NORTH;
		EAST.directionOpposite = WEST;
		WEST.directionOpposite = EAST;
	}
/**
 * 
 * @param current current direction
 * @param dirX x direction
 * @param dirY y direction
 */
	private Directions(int current, int dirX, int dirY) {
		this.current = current;
		this.directionX = dirX;
		this.directionY = dirY;
	}
};
