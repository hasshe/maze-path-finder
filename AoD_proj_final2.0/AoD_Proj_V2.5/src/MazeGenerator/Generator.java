package MazeGenerator;
import java.util.Collections;
import java.util.Scanner;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
 
public class Generator {
	private int width;
	private int height;
	private static int[][] maze;
	private String content = "";
	private String bottomWall = "";
 
	/**
	 * 
	 * @param width input width of maze
	 * @param height input height of maze
	 */
	public Generator(int width, int height) {
		this.width = width;
		this.height = height;
		maze = new int[this.width][this.height];
		generateMaze(0, 0);
	}
	/**
	 * Writes the maze data to the selected .txt file
	 * @param data data to be written to the file
	 * @throws IOException
	 */
	public static void writer(Object data) throws IOException
	{
		FileWriter fileWriter = new FileWriter("src/maze4.txt");
	    String fileContent = data.toString();
	     
	    fileWriter.write(fileContent);
	    fileWriter.close();
	}
 /**
  * Prints the maze into the console
 * @throws IOException 
  */
	public void printer() throws IOException {
		for (int yAxis = 0; yAxis < height; yAxis++) {
			for (int xAxis = 0; xAxis < width; xAxis++) {
				if(xAxis == width/2 && yAxis == 0) {
					System.out.print((maze[xAxis][yAxis] & 1) == 0 ? "S" : "S");
					content += ((maze[xAxis][yAxis] & 1) == 0 ? "S" : "S");
				}
				System.out.print((maze[xAxis][yAxis] & 1) == 0 ? "####" : "#   ");
				content +=((maze[xAxis][yAxis] & 1) == 0 ? "####" : "#   ");
			}
			System.out.println("#");
			content += "\n#";
			for (int xAxis = 0; xAxis < width; xAxis++) {
				System.out.print((maze[xAxis][yAxis] & 8) == 0 ? "   #" : "    ");
				content += ((maze[xAxis][yAxis] & 8) == 0 ? "  # " : "    ");
			}
			System.out.println("#");
			content += "\n ";
		}
		for (int xAxis = 0; xAxis < width; xAxis++) {
			if(xAxis == width/2) {
				System.out.print("E");
				bottomWall += "E";
			}
			System.out.print("####");
			bottomWall += "####";
		}
		bottomWall = bottomWall.substring(1,  bottomWall.length());
		content += bottomWall;
		writer(content);
	}
 /**
  * This class generates a position based on coordinates. These coordinates are generated at random with
  * the Collections.shuffle()
  * @param currentX current location at the x-axis
  * @param currentY current location at the y-axis
  */
	private void generateMaze(int currentX, int currentY) {
		Directions[] dirs = Directions.values();
		Collections.shuffle(Arrays.asList(dirs));
		for (Directions dir : dirs) {
			int newX = currentX + dir.directionX;
			int newY = currentY + dir.directionY;
			if (newX >= 0 && newX < width && newY >= 0 && height > newY && (maze[newX][newY] == 0)) {
				maze[currentX][currentY] += dir.current;
				maze[newX][newY] += dir.directionOpposite.current;
				generateMaze(newX, newY);
			}
		}
	}
 
}