package MazeGenerator;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

public class Main {
	
	/**
	 * Asks the user for an inout for height and width for a new generated maze
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		Scanner sc = new Scanner(System.in);
    	System.out.println("Enter Height of maze: ");
    	int height = sc.nextInt();
    	System.out.println("Enter Width of maze: ");
    	int width = sc.nextInt();
    	sc.close();
		Generator maze = new Generator(width, height);
		maze.printer();
		//maze.usingFileWriter();
	}

}
