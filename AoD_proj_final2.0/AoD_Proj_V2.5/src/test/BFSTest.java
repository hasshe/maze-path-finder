package test;

import static org.junit.Assert.*;


import java.io.File;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import mazePathFinder.Coordinate;
import mazePathFinder.Maze;
import mazePathFinder.*;

public class BFSTest {
	
	File mazeTest;
	Maze maze;
	Driver BFS;
	
	@Before
	public void setUp() throws Exception {
		mazeTest = new File("src/maze3.txt");
		maze = new Maze(mazeTest);
		BFS = new Driver();
	}
	@After
	public void tearDown() {
		mazeTest = null;
		maze = null;
		BFS = null;
	}

	@Test
	public void testCoordinatesVisited() {
		List<Coordinate> fastestPath = BFS.bfsPathFinder(maze);
		int x = 1;
		int y = 3;
		assertTrue("Coordinate should be marked as visited", maze.visitedCoordinates(x, y));
	}
	@Test
	public void testCoordinateIsWall() {
		List<Coordinate> fastestPath = BFS.bfsPathFinder(maze);
		int x = 1;
		int y = 1;
		assertTrue("Coordinate should be marked as a wall", maze.isWall(x, y));
	}
	@Test
	public void testIfLocationIsValid () {
		List<Coordinate> fastestPath = BFS.bfsPathFinder(maze);
		int x = 12;
		int y = 1;
		assertFalse("Coordinate should not be valid! Out of Bounds", maze.isLocationValid(x, y));
	}
	@Test
	public void testStartCoordinates() {
		List<Coordinate> fastestPath = BFS.bfsPathFinder(maze);
		int x = 0;
		int y = 3;
		assertTrue("Coordinate should be start coordinates", maze.isStartCoordinates(x, y));
	}
	@Test
	public void testExitCoordinates() {
		List<Coordinate> fastestPath = BFS.bfsPathFinder(maze);
		int x = 3;
		int y = 3;
		assertTrue("Coordinate should be exit coordinates", maze.isExitCoordinates(x, y));
	}
	@Test
	public void testMazeHeight() {
		List<Coordinate> fastestPath = BFS.bfsPathFinder(maze);
		assertTrue("Height should be 5", maze.getMazeHeight() == 5);
	}
	@Test
	public void testMazeWidth() {
		List<Coordinate> fastestPath = BFS.bfsPathFinder(maze);
		assertTrue("Width should be 7", maze.getMazeWidth() == 7);
	}

}
