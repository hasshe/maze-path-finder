package se.hig.aod.lab2;

public class ListNod {
	
	private Object data;
	public ListNod next;
	
	/**
	 * 
	 * @param data data som sedan finner sig i listan
	 * @param next pekar på noder i listan
	 */

	protected ListNod(Object data, ListNod next) {
		this.data = data;
		this.next = next;
	}
	
	/**
	 * 
	 * @param data data som sedan finner sig i listan
	 */
	protected ListNod(Object data) {
		this.data = data;
	}
	
	/**
	 * 
	 * @return data som finns i listan
	 */
	protected Object getData()
	{
		return data;
	}
	
	/**
	 * 
	 * @return vilken nod som är näst i listan
	 */
	protected ListNod getNext() {
		return next;
	}
	
	/**
	 * 
	 * @param data data som läggs till i en nod i listan
	 */
	protected void setData(Object data) {
		this.data = data;
	}
	
	/**
	 * Skriver ut noderna i listan
	 */
	protected void printLink() {
	   System.out.print("{" + data + "} ");
	}
	
	/**
	 * Skriver ut noderna i listan rekursivt
	 * @param nod tar emot en nod med data och pekare
	 * 
	 */
	protected void printR(ListNod nod) {
		if(nod.next != null) {
			System.out.println(nod.getData());
			nod = nod.next;
			printR(nod);
		}
		else {
			System.out.println(nod.getData());
		}
			
	}

}
