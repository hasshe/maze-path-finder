package se.hig.aod.lab2;

/**
 * De implementerade metoderna för Lsit ADT. Denna klass skall lägga till, ta bort och redovisa innehåll i den Länkade Listan.
 * 
 * UPDATE: Funktionerna för get, insert och remove finns nu också med. Dessa tillåter användaren att lägga till, ta bort eller hämta data
 * från länkade listan från en specifik position.
 */
import se.hig.aod.lab1.ListStack;
import se.hig.aod.lab1.Stack;

public class LinkedList<T> implements IndexedList<T> {
	
	private ListNod first;
	private int i = 0;
	private Stack<Object> st = new ListStack<Object> ();
	/**
	 * Konstruktorn som skapar en ny tom lista
	 */
	public LinkedList () {
		first = null;
	}
/**
 * Boolsk metod som kontrollerar ifall en lista är tom eller inte
 * 
 * @return ifall listan är tom eller inte 
 */
	@Override
	public boolean isEmpty() {
		
		return first == null;
	}
/**
 * Rensar en lista från all data
 */
	@Override
	public void clear() {
		while(first != null) {
			first = null;
			i = 0;
		}
		
	}
/**
 * @return antal element i en lista
 */
	@Override
	public int numberOfElements() {
		return i;
	}
/**
 * @param t Data som metoden tar in och sätter först i listan. Inkrementerar sedan antal element i listan
 */
	@Override
	public void insertFirst(T t) {
		ListNod insertNod = new ListNod(t);
		insertNod.next = first;
		
		first = insertNod;
		i++;
		
	}
/**
 * @param t Data som tas emot och läggs sist i listan. Använder sig av en Stack för att lägga till nya objekt sist i listan
 */
	@Override
	public void insertLast(T t) {
		ListNod nod = first;
		
		while(nod.next!= null ) {
			nod = nod.next;
			if(nod.next == null) {
				ListNod nod2 = new ListNod(t);
				nod2.next = null;
				nod.next = nod2;
				i++;
				return;
				
			}
		}
	}
/**
 * Tar bort det element som finns först i listan och skiftar alla element i listan ett steg till vänster. Decrementerar antal element i en lista
 * @return första elementet i listan
 */
	@SuppressWarnings("unchecked")
	@Override
	public T removeFirst() {
		ListNod removeNod = first;
		if(first == null) {
			throw new ListIsEmptyException("The list is empty");
		}
		first = first.next;
		i--;
		return (T)removeNod;
	}
	
/**
 * Tar bort det element som finns sist i en lista via användning av en Stack.
 * 
 * @return sista noden i listan
 */
	@SuppressWarnings("unchecked")
	@Override
	public T removeLast() {
		if(first == null) {
			throw new ListIsEmptyException("List is empty");
		}
		int pos = numberOfElements()-1;
		ListNod temp = first;
		for(int i=0; i< pos - 1 && temp.next != null; i++)
		{
			temp = temp.next;
		}
		temp.next = temp.next.next;
		i--;
		return (T)temp;
	}
/**
 * Hämtar och redovisar det element som finns först i listan utan att ta bort den
 * 
 * @return det data som noden först i listan innehålerr
 */
	@SuppressWarnings("unchecked")
	@Override
	public T getFirst() {
		ListNod nod  = first;
		if(first == null) {
			throw new ListIsEmptyException("The list is empty");
		}

		return (T)nod.getData();
	}
/**
 * Hämtar och redovisar det element som finns sist i listan utan att ta bort den
 * 
 *  @return det data som noden sist i listan innehåller
 */
	@SuppressWarnings("unchecked")
	@Override
	public T getLast() {
		ListNod nod = first;
		if(first == null) {
			throw new ListIsEmptyException("The list is empty");
		}
		while(nod.next != null) {
			nod = nod.next;
			Object t = nod.getData();
			if(nod.next == null) {
				return (T) t;
			}
		}
		return (T)nod.getData();
	}
/**
 * @param t Värde som kontrolleras ifall den existerar i listan. Itererar igenom hela listan tills rätt värde hittar och returnerar SANT eller FALSKT
 * 
 * @return SANT eller FALSKT ifall ett värde finns i listan eller inte
 */
	@Override
	public boolean contains(T t) {
		ListNod nod = first;
		if(first == null) {
			throw new ListIsEmptyException("The list is empty");
		}
		while(nod.getData() != null) {
			if(nod.getData() == t) {
				System.out.println("MACTH!");
				return true;
			}
			else if(nod.getData() != t){
				System.out.println("NO MACTH!");
				nod = nod.next;
			}
		}
		return false;
	}
/**
 * Skriver ut alla element som finns i listan
 */
	@Override
	public void printList() {
		ListNod cld = first;
		if(first == null) {
			throw new ListIsEmptyException("The list is empty");
		}
		System.out.println("List: ");
		while(cld != null) {
			cld.printLink();
			cld = cld.next;
		}
		System.out.print("");
	}
/**
 * Skriver ut listan rekursivt
 */
	@Override
	public void printListR() {
		ListNod cld = first;
		cld.printR(first);
	}
/**
 * Skriver ut alla element i listan i motsatt ordning. Använder sig av en Stack för att skriva ut
 */
	@Override
	public void reversePrintList() {
		ListNod nod = first;
		if(first == null) {
			throw new ListIsEmptyException("The list is empty");
		}
		while(nod.next!= null ) {
			st.push(nod.getData());
			nod = nod.next;
			if(nod.next == null) {
				st.push(nod.getData());
				while(st.isEmpty() == false) {
					@SuppressWarnings("unchecked")
					T y = (T) st.pop();
					System.out.println(y);
				}
				break;
			}
		}
	}
	/**
	 * @param t värde som skall läggas till i listan
	 * @param pos Positionen där värdet skall läggas i listan
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void insert(T t, int pos) {
		ListNod nod = first;
		int i = 0;
		if(pos == 0) {
			insertFirst(t);
			return;
		}
		else if(pos == 1) {
			ListNod nod2 = new ListNod(t);
			nod2.next = nod.next;
			nod.next = nod2;
			i++;
			return;
		}
		while(nod.next!= null && i != pos-1 ) {
			nod = nod.next;
			i++;
			if(nod.next == null || i == pos-1) {
				ListNod nod2 = new ListNod(t);
				nod2.next = nod.next;
				nod.next = nod2;
				i++;
				return;
				
			}
		}
	}
	/**
	 * @param pos Positionen för ett element att ta bort
	 * @return noden som tagits bort från listan
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public T remove(int pos) {
		if(first == null) {
			throw new ListIsEmptyException("List is empty");
		}
		ListNod temp = first;
		for(int i=0; i< pos - 1 && temp.next != null; i++)
		{
			temp = temp.next;
		}
		temp.next = temp.next.next;
		i--;
		return (T)temp;
	}
	
	/**
	 * @param pos En position som tas emot för att hämta ett element från en given position
	 * @return datat som finns i noden som ska hämtas
	 * 
	 * Metoden tar emot en position där ett element finns och returnerar det elementet för att redovisas. 
	 */
	@SuppressWarnings("unchecked")
	@Override
	
	public T get(int pos) {
		if(first == null) {
			throw new ListIsEmptyException("List is empty");
		}
		ListNod nod = first;
		int i = 0;
		if(pos == 0) {
			getFirst();
		}
		else if(pos == 1) {
			nod = nod.next;
			return (T)nod.getData();
		}
		while(nod.next!= null && i != pos ) {
			nod = nod.next;
			i++;
			if(nod.next == null || i == pos) {
				
				return (T)nod.getData();
				
			}
		}
		return (T)nod.getData();
	}
		
}